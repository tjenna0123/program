#1
def countdown(n):
     if n <= 0:
          print('Blastoff!')
     else:
          print(n)
          countdown(n-1)
          
def countup(n):
    if n >= 0:
        print('Blastoff!')
    else:
        print(n)
        countup(n+1)

num=int(input("Nhap so: "))
if num > 0:
        countdown(num)
elif num < 0:
        countup(num)
else:
        print('Blastoff!')

#2
gia = int(input("nhap gia hang: "))
nhan = int(("nhap tien nhan: "))
sum = (nhan - gia)
print('tien thoi lai la ', sum)

#ValueError: invalid literal for int() with base 10: 'nhap tien nhan: '
#Fix
gia = int(input("nhap gia hang: "))
nhan = int(input("nhap tien nhan: "))
sum = (nhan - gia)
print('tien thoi lai la ', sum)
